Pod::Spec.new do |spec|
  spec.name         = "podTool"
  spec.version      = "0.1.0"
  spec.summary      = "It's a test for create pod repo."

  spec.description  = <<-DESC
                    测试pod工具，用于尝试常见pod库的操作流程It's a test for create pod repo.
                   DESC

  spec.homepage     = "https://zkk.me"
  spec.author             = { "ZKK" => "zhang.kangkang@outlook.com" }
  spec.social_media_url   = "https://zkk.me"

  spec.platform     = :ios
  spec.platform     = :ios, "5.0"

  spec.source       = { :git => "https://gitee.com/deepwidth/pod.git", :tag => "#{spec.version}" }

  spec.source_files  = "podTool", "podTool/**/*.{h,m}"

  spec.requires_arc = true

end
