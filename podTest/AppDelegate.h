//
//  AppDelegate.h
//  podTest
//
//  Created by zhangkangkang on 2020/11/7.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end
