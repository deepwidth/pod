//
//  podTool.h
//  podTest
//
//  Created by zhangkangkang on 2020/11/7.
//

#import <UIKit/UIKit.h>

@interface PodTool : NSObject

/**获取字符串长度（Pod库创建测试）*/
+ (NSInteger)getStringLength:(NSString *)string;
@end
