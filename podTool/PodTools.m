//
//  PodTools.m
//  podTest
//
//  Created by zhangkangkang on 2020/11/7.
//

#import "PodTool.h"

@implementation PodTool

///获取字符串长度
+ (NSInteger)getStringLength:(NSString *)string {
    if(![string isKindOfClass:[NSString class]]) {
        return 0;
    }
    return string.length;
}

@end
